package com.wunder.challenge.mvp.ui.splash;

import com.vanniktech.rxpermission.RxPermission;
import com.wunder.challenge.mvp.data.DataManager;
import com.wunder.challenge.mvp.utils.rx.TestSchedulerProvider;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.TestScheduler;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

/**
 * Created by ELOUALI .
 */
@RunWith(MockitoJUnitRunner.Silent.class)
public class SplashPresenterTest {

    @Mock
    SplashMvpView mMockSplashMvpView;
    @Mock
    DataManager mMockDataManager;
    @Mock
    RxPermission rxPermission;
    private SplashPresenter<SplashMvpView> mSplashPresenter;
    private TestScheduler mTestScheduler;


    @Before
    public void setUp() {
        CompositeDisposable compositeDisposable = new CompositeDisposable();
        mTestScheduler = new TestScheduler();
        TestSchedulerProvider testSchedulerProvider = new TestSchedulerProvider(mTestScheduler);
        mSplashPresenter = new SplashPresenter<>(
                mMockDataManager,
                testSchedulerProvider,
                compositeDisposable);
        mSplashPresenter.onAttach(mMockSplashMvpView);
    }

    @Test
    public void testCheckPermissionSuccess() {
        doReturn(rxPermission).when(mMockDataManager).getPermission();
        verify(mMockSplashMvpView, never()).openMainActivity();
        verifyNoMoreInteractions(mMockSplashMvpView);
    }

    @After
    public void tearDown() {
        mSplashPresenter.onDetach();
    }


}