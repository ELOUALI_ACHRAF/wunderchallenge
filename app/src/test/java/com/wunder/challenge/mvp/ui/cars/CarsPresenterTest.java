package com.wunder.challenge.mvp.ui.cars;

import android.location.Location;

import com.wunder.challenge.mvp.data.DataManager;
import com.wunder.challenge.mvp.data.network.model.CarResponse;
import com.wunder.challenge.mvp.utils.rx.TestSchedulerProvider;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.TestScheduler;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

/**
 * Created by ELOUALI .
 */
@RunWith(MockitoJUnitRunner.class)
public class CarsPresenterTest {

    @Mock
    CarsMvpView mMockCarsMvpView;
    @Mock
    DataManager mMockDataManager;


    private CarsPresenter<CarsMvpView> mCarsPresenter;


    private TestScheduler mTestScheduler;


    @BeforeClass
    public static void onlyOnce() {
    }

    @Before
    public void setUp() {
        CompositeDisposable compositeDisposable = new CompositeDisposable();
        mTestScheduler = new TestScheduler();
        TestSchedulerProvider testSchedulerProvider = new TestSchedulerProvider(mTestScheduler);
        mCarsPresenter = new CarsPresenter<>(
                mMockDataManager,
                testSchedulerProvider,
                compositeDisposable);
        mCarsPresenter.onAttach(mMockCarsMvpView);
    }

    @Test
    public void testServerLoadCars() {
        List<CarResponse> carsResponseList = new ArrayList<>();
        doReturn(Observable.just(carsResponseList))
                .when(mMockDataManager)
                .getCarsApiCall();

        mCarsPresenter.getCarsFromServer();
        mTestScheduler.triggerActions();

        verify(mMockCarsMvpView).addCarsToMap(carsResponseList);
    }


    @Test
    public void testGetUserLocationSuccess() {
        Location locationResponse = new Location("GpsProvider");
        doReturn(Flowable.just(locationResponse)).when(mMockDataManager).getLocation();

        mCarsPresenter.onFetchUserLocation();
        mTestScheduler.triggerActions();

        verify(mMockCarsMvpView).updateUserLocation(locationResponse);
    }


}