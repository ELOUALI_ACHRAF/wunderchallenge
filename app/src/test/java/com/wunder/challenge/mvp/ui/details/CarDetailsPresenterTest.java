package com.wunder.challenge.mvp.ui.details;

import com.wunder.challenge.mvp.data.DataManager;
import com.wunder.challenge.mvp.data.network.model.CarDetailsResponse;
import com.wunder.challenge.mvp.data.network.model.QuickRentRequest;
import com.wunder.challenge.mvp.data.network.model.QuickRentResponse;
import com.wunder.challenge.mvp.utils.rx.TestSchedulerProvider;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import io.reactivex.Single;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.TestScheduler;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

/**
 * Created by ELOUALI .
 */
@RunWith(MockitoJUnitRunner.class)
public class CarDetailsPresenterTest {

    @Mock
    CarDetailsMvpView mMockLoginMvpView;
    @Mock
    DataManager mMockDataManager;

    private CarDetailsPresenter<CarDetailsMvpView> mCarDetailsPresenter;


    private TestScheduler mTestScheduler;


    @Before
    public void setUp() {
        CompositeDisposable compositeDisposable = new CompositeDisposable();
        mTestScheduler = new TestScheduler();
        TestSchedulerProvider testSchedulerProvider = new TestSchedulerProvider(mTestScheduler);
        mCarDetailsPresenter = new CarDetailsPresenter<>(
                mMockDataManager,
                testSchedulerProvider,
                compositeDisposable);
        mCarDetailsPresenter.onAttach(mMockLoginMvpView);
    }


    @Test
    public void testGetCarDetailsSuccess() {
        int carID = 6;
        CarDetailsResponse carDetailsResponse = new CarDetailsResponse();

        doReturn(Single.just(carDetailsResponse))
                .when(mMockDataManager)
                .getCarDetails(carID);

        mCarDetailsPresenter.onGetCarDetails(carID);
        mTestScheduler.triggerActions();

        verify(mMockLoginMvpView).showLoading();
        verify(mMockLoginMvpView).hideLoading();
        verify(mMockLoginMvpView).showCarDetails(carDetailsResponse);
        verifyNoMoreInteractions(mMockLoginMvpView);
    }

    @Test
    public void testQuickRentSuccess() {
        int carID = 6;
        QuickRentResponse quickRentResponse = new QuickRentResponse();

        doReturn(Single.just(quickRentResponse))
                .when(mMockDataManager)
                .doQuickRentApiCall(new QuickRentRequest
                        .ServerQuickRentRequest(carID));

        mCarDetailsPresenter.onDoQuicRent(carID);
        mTestScheduler.triggerActions();

        verify(mMockLoginMvpView).showLoading();
        verify(mMockLoginMvpView).hideLoading();
        verify(mMockLoginMvpView).showSuccessQuickRentMessage(quickRentResponse);
        verifyNoMoreInteractions(mMockLoginMvpView);
    }


    @After
    public void tearDown() {
        mCarDetailsPresenter.onDetach();
    }


}