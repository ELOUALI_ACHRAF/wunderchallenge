package com.wunder.challenge.mvp.ui.cars;

import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.intent.rule.IntentsTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.test.uiautomator.UiDevice;
import android.support.test.uiautomator.UiObject;
import android.support.test.uiautomator.UiObjectNotFoundException;
import android.support.test.uiautomator.UiSelector;

import com.wunder.challenge.mvp.R;
import com.wunder.challenge.mvp.TestComponentRule;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.junit.rules.TestRule;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

/**
 * Created by ELOUALI .
 */
@RunWith(AndroidJUnit4.class)
public class CarsActivityTest {

    private final TestComponentRule component =
            new TestComponentRule(InstrumentationRegistry.getTargetContext());

    private final IntentsTestRule<CarsActivity> main =
            new IntentsTestRule<>(CarsActivity.class, false, false);
    @Rule
    public TestRule chain = RuleChain.outerRule(component).around(main);
    private UiDevice device;

    @Before
    public void setup() {
        device = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());
    }


    @Test
    public void TestGoolgeMapIsVisible() {
        main.launchActivity(CarsActivity.getStartIntent(component.getContext()));
        onView(withId(R.id.map))
                .check(matches(isDisplayed()));
    }

    @Test
    public void TestUserLocationMarkerIsVisible() {
        main.launchActivity(CarsActivity.getStartIntent(component.getContext()));
        UiObject marker = device.findObject(new UiSelector().descriptionContains("Your location"));
        try {
            marker.click();
        } catch (UiObjectNotFoundException e) {
            e.printStackTrace();
        }

    }


    @Test
    public void TestCarMarkerIsVisible() {
        main.launchActivity(CarsActivity.getStartIntent(component.getContext()));
        UiObject marker = device.findObject(new UiSelector().descriptionContains("Fanny"));
        try {
            marker.click();
        } catch (UiObjectNotFoundException e) {
            e.printStackTrace();
        }
    }

}