package com.wunder.challenge.mvp.ui.details;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.action.ViewActions;
import android.support.test.espresso.intent.rule.IntentsTestRule;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.runner.AndroidJUnit4;

import com.wunder.challenge.mvp.R;
import com.wunder.challenge.mvp.TestComponentRule;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.junit.rules.TestRule;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withEffectiveVisibility;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

/**
 * Created by ELOUALI.
 */
@RunWith(AndroidJUnit4.class)
public class CarDetailsActivityTest {

    private final TestComponentRule component =
            new TestComponentRule(InstrumentationRegistry.getTargetContext());

    private final IntentsTestRule<CarDetailsActivity> main =
            new IntentsTestRule<>(CarDetailsActivity.class, false, false);

    @Rule
    public TestRule chain = RuleChain.outerRule(component).around(main);

    @Before
    public void setup() {
    }


    @Test
    public void TestCarDetailsView() {
        main.launchActivity(CarDetailsActivity.getStartIntent(component.getContext()));
        onView(withId(R.id.tv_car_title))
                .check(matches(isDisplayed()));
        onView(withId(R.id.tv_car_isclean))
                .check(matches(isDisplayed()));
        onView(withId(R.id.tv_car_isdamaged))
                .check(matches(isDisplayed()));

        onView(withText("Quick rent"))
                .check(matches(isDisplayed()));
        onView(withText("Quick rent"))
                .perform(ViewActions.click());

        onView(withText(getResourceString(R.string.api_default_error)))
                .check(matches(withEffectiveVisibility(
                        ViewMatchers.Visibility.VISIBLE
                )));
    }

    private String getResourceString(int id) {
        Context targetContext = InstrumentationRegistry.getTargetContext();
        return targetContext.getResources().getString(id);
    }

}