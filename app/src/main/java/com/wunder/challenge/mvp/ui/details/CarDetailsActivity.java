package com.wunder.challenge.mvp.ui.details;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.wunder.challenge.mvp.R;
import com.wunder.challenge.mvp.data.network.model.CarDetailsResponse;
import com.wunder.challenge.mvp.data.network.model.QuickRentResponse;
import com.wunder.challenge.mvp.ui.base.BaseActivity;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class CarDetailsActivity extends BaseActivity implements CarDetailsMvpView, OnMapReadyCallback {

    @Inject
    CarDetailsMvpPresenter<CarDetailsMvpView> mPresenter;


    @BindView(R.id.rl_car_photo)
    RelativeLayout rl_car_photo;

    @BindView(R.id.iv_car_photo)
    ImageView iv_car_photo;

    @BindView(R.id.pb_load_car_photo)
    ProgressBar pb_load_car_photo;

    @BindView(R.id.tv_car_title)
    TextView tv_car_title;

    @BindView(R.id.tv_car_isclean)
    TextView tv_car_is_clean;

    @BindView(R.id.tv_car_isdamaged)
    TextView tv_car_is_damaged;

    @BindView(R.id.tv_car_isdamaged_description)
    TextView tv_car_is_damaged_description;

    @BindView(R.id.ll_damage_description_layout)
    LinearLayout ll_damage_description_layout;

    @BindView(R.id.tv_car_licence)
    TextView tv_car_licence;

    @BindView(R.id.tv_car_fuel)
    TextView tv_car_fuel;

    @BindView(R.id.pb_car_fuel)
    ProgressBar pb_car_fuel;

    @BindView(R.id.tv_car_stateid)
    TextView tv_car_state_id;

    @BindView(R.id.tv_car_hardwareid)
    TextView tv_car_hardware_id;

    @BindView(R.id.tv_car_type_id)
    TextView tv_car_type_id;

    @BindView(R.id.tv_car_pricing_time)
    TextView tv_car_pricing_time;

    @BindView(R.id.tv_car_pricing_parking)
    TextView tv_car_pricing_parking;

    @BindView(R.id.tv_is_activated_by_hardware)
    TextView tv_is_activated_by_hardware;

    @BindView(R.id.tv_address)
    TextView tv_address;

    @BindView(R.id.tv_zip_code)
    TextView tv_zip_code;

    @BindView(R.id.tv_city)
    TextView tv_city;

    @BindView(R.id.tv_reservation_state)
    TextView tv_reservation_state;


    boolean mMapReady = false;
    GoogleMap mMap;
    int carId;

    public static Intent getStartIntent(Context context) {
        Intent intent = new Intent(context, CarDetailsActivity.class);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent mIntent = getIntent();
        carId = mIntent.getIntExtra("carId", 0);

        setContentView(R.layout.activity_car_details);
        getActivityComponent().inject(this);
        setUnBinder(ButterKnife.bind(this));
        mPresenter.onAttach(CarDetailsActivity.this);
        mPresenter.onGetCarDetails(carId);


        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);


    }

    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }

    @Override
    public void showCarDetails(CarDetailsResponse carDetailsResponse) {
        renderPhoto(carDetailsResponse.getVehicleTypeImageUrl());
        renderTitle(carDetailsResponse.getTitle());
        renderIsClean(carDetailsResponse.getIsClean());
        renderIsDamaged(carDetailsResponse.getIsDamaged(), carDetailsResponse.getDamageDescription());
        renderLicence(carDetailsResponse.getLicencePlate());
        renderFuel(carDetailsResponse.getFuelLevel());
        renderStateID(carDetailsResponse.getVehicleStateId());
        renderHardwareID(carDetailsResponse.getHardwareId());
        renderTypeID(carDetailsResponse.getVehicleTypeId());
        renderPricingTime(carDetailsResponse.getPricingTime());
        renderPricingParking(carDetailsResponse.getPricingParking());
        renderActivatedByHardware(carDetailsResponse.getIsActivatedByHardware());
        renderAddress(carDetailsResponse.getAddress());
        renderZipCode(carDetailsResponse.getZipCode());
        renderCity(carDetailsResponse.getCity());
        renderMap(carDetailsResponse.getLat(), carDetailsResponse.getLon());
        renderReservationState(carDetailsResponse.getReservationState());

    }

    public void renderPhoto(String carPhotoUrl) {

        if (carPhotoUrl != null) {
            pb_load_car_photo.setVisibility(View.VISIBLE);
            Glide.with(this)
                    .load(carPhotoUrl)
                    .listener(new RequestListener<String, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                            pb_load_car_photo.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                            pb_load_car_photo.setVisibility(View.GONE);
                            return false;
                        }
                    })
                    .centerCrop()
                    .into(iv_car_photo);

        } else {
            rl_car_photo.setVisibility(View.GONE);
        }
    }

    public void renderTitle(String carTitle) {
        if (carTitle != null && !carTitle.equals(getResources().getString(R.string.empty_string))) {
            tv_car_title.setText(carTitle);
        } else {
            tv_car_title.setText(getResources().getString(R.string.no_value));
        }
    }

    public void renderIsClean(boolean isClean) {
        if (isClean) {
            tv_car_is_clean.setText(getResources().getString(R.string.yes_string));
        } else {
            tv_car_is_clean.setText(getResources().getString(R.string.no_string));
        }
    }

    public void renderIsDamaged(boolean isDamaged, String damageDescription) {
        if (isDamaged) {
            tv_car_is_damaged.setText(getResources().getString(R.string.yes_string));
            if (damageDescription != null) {
                tv_car_is_damaged_description.setText(damageDescription);
            } else {
                tv_car_is_damaged_description.setText(getResources().getString(R.string.no_value));
            }
        } else {
            tv_car_is_damaged.setText(getResources().getString(R.string.no_string));
            ll_damage_description_layout.setVisibility(View.GONE);
        }
    }

    public void renderLicence(String licence) {
        if (licence != null && !licence.equals(getResources().getString(R.string.empty_string))) {
            tv_car_licence.setText(licence);
        } else {
            tv_car_licence.setText(getResources().getString(R.string.no_value));
        }
    }

    public void renderFuel(Integer fuelLevel) {
        if (fuelLevel != null) {
            tv_car_fuel.setText(fuelLevel + "%");
            pb_car_fuel.setProgress(fuelLevel);
        } else {
            tv_car_fuel.setText(getResources().getString(R.string.no_value));
            pb_car_fuel.setVisibility(View.GONE);
        }
    }

    public void renderStateID(Integer stateID) {
        if (stateID != null) {
            tv_car_state_id.setText(stateID.toString());
        } else {
            tv_car_state_id.setText(getResources().getString(R.string.no_value));
        }
    }

    public void renderHardwareID(String hardwareID) {
        if (hardwareID != null && !hardwareID.equals(getResources().getString(R.string.empty_string))) {
            tv_car_hardware_id.setText(hardwareID);
        } else {
            tv_car_hardware_id.setText(getResources().getString(R.string.no_value));
        }
    }

    public void renderTypeID(Integer typeID) {
        if (typeID != null) {
            tv_car_type_id.setText(typeID.toString());
        } else {
            tv_car_type_id.setText(getResources().getString(R.string.no_value));
        }
    }

    public void renderPricingTime(String pricingTime) {
        if (pricingTime != null && !pricingTime.equals(getResources().getString(R.string.empty_string))) {
            tv_car_pricing_time.setText(pricingTime.toString());
        } else {
            tv_car_pricing_time.setText(getResources().getString(R.string.no_value));
        }
    }

    public void renderPricingParking(String pricingParking) {
        if (pricingParking != null && !pricingParking.equals(getResources().getString(R.string.empty_string))) {
            tv_car_pricing_parking.setText(pricingParking.toString());
        } else {
            tv_car_pricing_parking.setText(getResources().getString(R.string.no_value));
        }
    }

    public void renderActivatedByHardware(boolean activatedByHardware) {
        if (activatedByHardware) {
            tv_is_activated_by_hardware.setText(getResources().getString(R.string.yes_string));
        } else {
            tv_is_activated_by_hardware.setText(getResources().getString(R.string.no_string));
        }
    }

    public void renderAddress(String address) {
        if (address != null && !address.equals(getResources().getString(R.string.empty_string))) {
            tv_address.setText(address);
        } else {
            tv_address.setText(getResources().getString(R.string.no_value));
        }
    }

    public void renderZipCode(String zipCode) {
        if (zipCode != null && !zipCode.equals(getResources().getString(R.string.empty_string))) {
            tv_zip_code.setText(zipCode);
        } else {
            tv_zip_code.setText(getResources().getString(R.string.no_value));
        }
    }

    public void renderCity(String city) {
        if (city != null && !city.equals(getResources().getString(R.string.empty_string))) {
            tv_city.setText(city);
        } else {
            tv_city.setText(getResources().getString(R.string.no_value));
        }
    }

    public void renderMap(double latitude, double longitude) {
        if (mMapReady && latitude != 0.0) {

            mMap.getUiSettings().setZoomControlsEnabled(true);
            mMap.getUiSettings().setMyLocationButtonEnabled(true);
            Marker carLocationMarker = mMap.addMarker(new MarkerOptions()
                    .position(new LatLng(latitude, longitude)));
            new Handler().postDelayed(() -> mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(carLocationMarker.getPosition(), 16.0f)), 300);

        }
    }

    public void renderReservationState(Integer reservationState) {
        if (reservationState != null) {
            tv_reservation_state.setText(reservationState.toString());
        } else {
            tv_reservation_state.setText(getResources().getString(R.string.no_value));
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMapReady = true;
        mMap = googleMap;

        try {
            mMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(this, R.raw.style_json));

        } catch (Resources.NotFoundException e) {
        }
    }

    @OnClick(R.id.btn_quick_rent)
    void onDoQuickRent(View v) {
        mPresenter.onDoQuicRent(carId);
    }

    @OnClick(R.id.btn_go_back)
    void onGoBackClicked(View v) {
        onBackPressed();
    }

    @Override
    public void showSuccessQuickRentMessage(QuickRentResponse quickRentResponse) {
        Toast.makeText(this, getResources().getString(R.string.quick_rent_success) + quickRentResponse.getReservationId(), Toast.LENGTH_LONG).show();
        finish();
    }
}
