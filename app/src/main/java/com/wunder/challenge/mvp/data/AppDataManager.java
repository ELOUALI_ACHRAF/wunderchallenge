package com.wunder.challenge.mvp.data;


import android.content.Context;
import android.location.Location;

import com.vanniktech.rxpermission.RxPermission;
import com.wunder.challenge.mvp.data.location.LocationHelper;
import com.wunder.challenge.mvp.data.network.ApiHelper;
import com.wunder.challenge.mvp.data.network.model.CarDetailsResponse;
import com.wunder.challenge.mvp.data.network.model.CarResponse;
import com.wunder.challenge.mvp.data.network.model.QuickRentRequest;
import com.wunder.challenge.mvp.data.network.model.QuickRentResponse;
import com.wunder.challenge.mvp.di.ApplicationContext;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.Single;


@Singleton
public class AppDataManager implements DataManager {


    private final Context mContext;
    private final ApiHelper mApiHelper;
    private final LocationHelper mLocationHelper;


    @Inject
    public AppDataManager(@ApplicationContext Context context,
                          ApiHelper apiHelper, LocationHelper locationHelper) {
        mContext = context;
        mApiHelper = apiHelper;
        mLocationHelper = locationHelper;
    }


    @Override
    public Flowable<Location> getLocation() {
        return mLocationHelper.getLocation();
    }


    @Override
    public RxPermission getPermission() {
        return mLocationHelper.getPermission();
    }


    @Override
    public Observable<List<CarResponse>> getCarsApiCall() {
        return mApiHelper.getCarsApiCall();
    }


    @Override
    public Single<CarDetailsResponse> getCarDetails(int carId) {
        return mApiHelper.getCarDetails(carId);
    }

    @Override
    public Single<QuickRentResponse> doQuickRentApiCall(QuickRentRequest.ServerQuickRentRequest request) {
        return mApiHelper.doQuickRentApiCall(request);
    }


}
