package com.wunder.challenge.mvp.ui.cars;

import android.location.Location;

import com.androidnetworking.error.ANError;
import com.wunder.challenge.mvp.data.DataManager;
import com.wunder.challenge.mvp.data.network.model.CarResponse;
import com.wunder.challenge.mvp.ui.base.BasePresenter;
import com.wunder.challenge.mvp.utils.rx.SchedulerProvider;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.observers.DisposableObserver;

/**
 * Created by ELOUALI ACHRAF on 05/09/2019.
 */

public class CarsPresenter<V extends CarsMvpView> extends BasePresenter<V>
        implements CarsMvpPresenter<V> {

    private Disposable disposable;

    @Inject
    public CarsPresenter(DataManager dataManager,
                         SchedulerProvider schedulerProvider,
                         CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }

    @Override
    public void onAttach(V mvpView) {
        super.onAttach(mvpView);
    }


    @Override
    public void getCarsFromServer() {
        getCompositeDisposable().add(getDataManager()
                .getCarsApiCall()
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribeWith(new DisposableObserver<List<CarResponse>>() {
                    @Override
                    public void onNext(List<CarResponse> list) {
                        //update Map
                        getMvpView().addCarsToMap(list);
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (!isViewAttached()) {
                            return;
                        }
                        getMvpView().hideLoading();

                        if (e instanceof ANError) {
                            ANError anError = (ANError) e;
                            handleApiError(anError);
                        }
                    }

                    @Override
                    public void onComplete() {
                        if (!isViewAttached()) {
                            return;
                        }
                        getMvpView().hideLoading();
                    }
                }));
    }

    @Override
    public void onFetchUserLocation() {

        disposable = getDataManager()
                .getLocation()
                .subscribe(new Consumer<Location>() {
                    @Override
                    public void accept(Location location) {

                        if (!isViewAttached()) {
                            return;
                        }

                        getMvpView().updateUserLocation(location);
                    }
                }, throwable -> {
                });
    }


    @Override
    public void onDetach() {
        super.onDetach();
        disposable.dispose();
    }


}
