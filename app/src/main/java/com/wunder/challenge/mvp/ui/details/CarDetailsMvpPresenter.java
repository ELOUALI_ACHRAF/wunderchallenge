package com.wunder.challenge.mvp.ui.details;


import com.wunder.challenge.mvp.di.PerActivity;
import com.wunder.challenge.mvp.ui.base.MvpPresenter;


@PerActivity
public interface CarDetailsMvpPresenter<V extends CarDetailsMvpView> extends MvpPresenter<V> {

    void onGetCarDetails(int carId);

    void onDoQuicRent(int carId);
}
