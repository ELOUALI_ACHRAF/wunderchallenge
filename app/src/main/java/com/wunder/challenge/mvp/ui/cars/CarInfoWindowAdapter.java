package com.wunder.challenge.mvp.ui.cars;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;
import com.wunder.challenge.mvp.R;
import com.wunder.challenge.mvp.data.network.model.CarResponse;
import com.wunder.challenge.mvp.utils.MapUtils;


public class CarInfoWindowAdapter implements GoogleMap.InfoWindowAdapter {

    private Context context;

    public CarInfoWindowAdapter(Context ctx) {
        context = ctx;
    }

    @Override
    public View getInfoWindow(Marker marker) {
        return null;
    }

    @Override
    public View getInfoContents(Marker marker) {
        View view = ((Activity) context).getLayoutInflater()
                .inflate(R.layout.custom_info_window, null);

        TextView tv_car_title = view.findViewById(R.id.tv_car_title);
        TextView tv_pricing_time = view.findViewById(R.id.tv_pricing_time);
        TextView tv_fuel_level = view.findViewById(R.id.tv_fuel_level);
        LinearLayout ll_fuel = view.findViewById(R.id.ll_fuel);
        LinearLayout ll_pricing = view.findViewById(R.id.ll_pricing);


        try {
            CarResponse carData = (CarResponse) marker.getTag();
            String car_title = MapUtils.getCarTitle(carData);
            tv_car_title.setText(car_title + "");

            if (carData.getPricingTime() != null && !carData.getPricingTime().equals(context.getResources().getString(R.string.empty_string))) {
                tv_pricing_time.setText(carData.getPricingTime().toString());
                ll_pricing.setVisibility(View.VISIBLE);
            } else {
                ll_pricing.setVisibility(View.GONE);
            }

            if (carData.getFuelLevel() != null) {
                tv_fuel_level.setText(carData.getFuelLevel() + "%");
                ll_fuel.setVisibility(View.VISIBLE);
            } else {
                ll_fuel.setVisibility(View.GONE);
            }


        } catch (Exception e) {
            ll_fuel.setVisibility(View.GONE);
            ll_pricing.setVisibility(View.GONE);
        }


        return view;
    }
}