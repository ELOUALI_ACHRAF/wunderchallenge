package com.wunder.challenge.mvp.di.component;

import com.wunder.challenge.mvp.di.PerActivity;
import com.wunder.challenge.mvp.di.module.ActivityModule;
import com.wunder.challenge.mvp.ui.cars.CarsActivity;
import com.wunder.challenge.mvp.ui.details.CarDetailsActivity;
import com.wunder.challenge.mvp.ui.splash.SplashActivity;

import dagger.Component;


@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = ActivityModule.class)
public interface ActivityComponent {

    void inject(CarDetailsActivity activity);

    void inject(SplashActivity activity);

    void inject(CarsActivity fragment);

}
