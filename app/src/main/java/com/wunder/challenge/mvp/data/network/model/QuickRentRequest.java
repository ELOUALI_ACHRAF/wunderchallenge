package com.wunder.challenge.mvp.data.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;


public class QuickRentRequest {

    private QuickRentRequest() {
    }

    public static class ServerQuickRentRequest {

        @Expose
        @SerializedName("carId")
        public int carId;


        public ServerQuickRentRequest(int carId) {
            this.carId = carId;
        }

        public int getCarId() {
            return carId;
        }

        public void setCarId(int carId) {
            this.carId = carId;
        }

        public JSONObject construtJsonBody() {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("carId", this.carId);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return jsonObject;
        }

        @Override
        public boolean equals(Object object) {
            if (this == object) return true;
            if (object == null || getClass() != object.getClass()) return false;

            ServerQuickRentRequest that = (ServerQuickRentRequest) object;

            if (carId == that.carId) {
                return true;
            } else {
                return false;
            }

        }


    }

}
