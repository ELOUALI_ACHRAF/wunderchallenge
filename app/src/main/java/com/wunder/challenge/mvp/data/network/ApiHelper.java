package com.wunder.challenge.mvp.data.network;

import com.wunder.challenge.mvp.data.network.model.CarDetailsResponse;
import com.wunder.challenge.mvp.data.network.model.CarResponse;
import com.wunder.challenge.mvp.data.network.model.QuickRentRequest;
import com.wunder.challenge.mvp.data.network.model.QuickRentResponse;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Single;


public interface ApiHelper {

    Single<QuickRentResponse> doQuickRentApiCall(QuickRentRequest.ServerQuickRentRequest request);

    Single<CarDetailsResponse> getCarDetails(int carId);

    Observable<List<CarResponse>> getCarsApiCall();
}
