package com.wunder.challenge.mvp.utils;

import android.content.Context;
import android.content.res.Resources;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.MarkerOptions;
import com.wunder.challenge.mvp.R;
import com.wunder.challenge.mvp.data.network.model.CarResponse;

/**
 * Created by ELOUALI Achraf on 10/09/2019.
 */

public class MapUtils {

    public static MarkerOptions carResponse2MarkerOptions(CarResponse carResponse) {
        return new MarkerOptions()
                .position(new LatLng(carResponse.getLat(), carResponse.getLon()))
                .title(getCarTitle(carResponse));
    }

    public static String getCarTitle(CarResponse carResponse) {
        String carTitle = carResponse.getTitle();
        if (carTitle == null || carTitle.equals("")) {
            carTitle = "CarID-" + carResponse.getCarId();
        }
        return carTitle;
    }

    public static void setGoogleMapParams(GoogleMap googleMap, Context ctx) {
        try {
            googleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(ctx, R.raw.style_json));

        } catch (Resources.NotFoundException e) {
        }
        try {
            googleMap.setMyLocationEnabled(true);
        } catch (SecurityException e) {

        }
        googleMap.getUiSettings().setZoomControlsEnabled(true);
        googleMap.getUiSettings().setMyLocationButtonEnabled(true);
    }


}
