package com.wunder.challenge.mvp.ui.cars;

import com.wunder.challenge.mvp.ui.base.MvpPresenter;

/**
 * Created by ELOUALI ACHRAF on 05/09/2019.
 */

public interface CarsMvpPresenter<V extends CarsMvpView>
        extends MvpPresenter<V> {

    void getCarsFromServer();

    void onFetchUserLocation();

}


