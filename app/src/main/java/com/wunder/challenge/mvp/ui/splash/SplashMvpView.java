package com.wunder.challenge.mvp.ui.splash;

import com.wunder.challenge.mvp.ui.base.MvpView;


public interface SplashMvpView extends MvpView {


    void openMainActivity();

    void showPermissionNotGrantedView();

}
