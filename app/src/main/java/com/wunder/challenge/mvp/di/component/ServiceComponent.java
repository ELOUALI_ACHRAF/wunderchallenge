package com.wunder.challenge.mvp.di.component;

import com.wunder.challenge.mvp.di.PerService;
import com.wunder.challenge.mvp.di.module.ServiceModule;
import com.wunder.challenge.mvp.service.SyncService;

import dagger.Component;


@PerService
@Component(dependencies = ApplicationComponent.class, modules = ServiceModule.class)
public interface ServiceComponent {

    void inject(SyncService service);

}
