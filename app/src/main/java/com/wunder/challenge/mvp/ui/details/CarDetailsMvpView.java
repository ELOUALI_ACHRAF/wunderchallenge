package com.wunder.challenge.mvp.ui.details;

import com.wunder.challenge.mvp.data.network.model.CarDetailsResponse;
import com.wunder.challenge.mvp.data.network.model.QuickRentResponse;
import com.wunder.challenge.mvp.ui.base.MvpView;


public interface CarDetailsMvpView extends MvpView {

    void showCarDetails(CarDetailsResponse carResponse);

    void showSuccessQuickRentMessage(QuickRentResponse quickRentResponse);
}
