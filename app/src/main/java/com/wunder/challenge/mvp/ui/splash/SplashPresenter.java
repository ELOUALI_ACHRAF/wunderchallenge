package com.wunder.challenge.mvp.ui.splash;

import android.Manifest;

import com.vanniktech.rxpermission.Permission;
import com.wunder.challenge.mvp.data.DataManager;
import com.wunder.challenge.mvp.ui.base.BasePresenter;
import com.wunder.challenge.mvp.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;


public class SplashPresenter<V extends SplashMvpView> extends BasePresenter<V>
        implements SplashMvpPresenter<V> {


    @Inject
    public SplashPresenter(DataManager dataManager,
                           SchedulerProvider schedulerProvider,
                           CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }


    @Override
    public void onLocationPermissionCheck() {
        getCompositeDisposable().add(getDataManager().getPermission().requestEach(Manifest.permission.ACCESS_FINE_LOCATION)
                .subscribe(new Consumer<Permission>() {
                    @Override
                    public void accept(@NonNull Permission Permission)
                            throws Exception {

                        if (Permission.state() == com.vanniktech.rxpermission.Permission.State.GRANTED) {
                            getMvpView().openMainActivity();
                        } else {
                            getMvpView().showPermissionNotGrantedView();
                        }
                    }
                }));
    }
}
