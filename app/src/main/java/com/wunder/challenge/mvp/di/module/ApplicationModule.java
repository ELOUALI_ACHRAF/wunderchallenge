package com.wunder.challenge.mvp.di.module;

import android.app.Application;
import android.content.Context;

import com.wunder.challenge.mvp.data.AppDataManager;
import com.wunder.challenge.mvp.data.DataManager;
import com.wunder.challenge.mvp.data.location.AppLocationHelper;
import com.wunder.challenge.mvp.data.location.LocationHelper;
import com.wunder.challenge.mvp.data.network.ApiHelper;
import com.wunder.challenge.mvp.data.network.AppApiHelper;
import com.wunder.challenge.mvp.di.ApplicationContext;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;


@Module
public class ApplicationModule {

    private final Application mApplication;

    public ApplicationModule(Application application) {
        mApplication = application;
    }

    @Provides
    @ApplicationContext
    Context provideContext() {
        return mApplication;
    }

    @Provides
    Application provideApplication() {
        return mApplication;
    }


    @Provides
    @Singleton
    DataManager provideDataManager(AppDataManager appDataManager) {
        return appDataManager;
    }


    @Provides
    @Singleton
    ApiHelper provideApiHelper(AppApiHelper appApiHelper) {
        return appApiHelper;
    }


    @Provides
    @Singleton
    LocationHelper provideLocationHelper(AppLocationHelper appLocationHelper) {
        return appLocationHelper;
    }


}
