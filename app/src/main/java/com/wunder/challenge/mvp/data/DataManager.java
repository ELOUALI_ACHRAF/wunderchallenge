package com.wunder.challenge.mvp.data;


import com.wunder.challenge.mvp.data.location.LocationHelper;
import com.wunder.challenge.mvp.data.network.ApiHelper;

public interface DataManager extends ApiHelper, LocationHelper {
}
