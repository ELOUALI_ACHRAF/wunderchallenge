package com.wunder.challenge.mvp.ui.details;

import com.androidnetworking.error.ANError;
import com.wunder.challenge.mvp.data.DataManager;
import com.wunder.challenge.mvp.data.network.model.CarDetailsResponse;
import com.wunder.challenge.mvp.data.network.model.QuickRentRequest;
import com.wunder.challenge.mvp.data.network.model.QuickRentResponse;
import com.wunder.challenge.mvp.ui.base.BasePresenter;
import com.wunder.challenge.mvp.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;


public class CarDetailsPresenter<V extends CarDetailsMvpView> extends BasePresenter<V>
        implements CarDetailsMvpPresenter<V> {


    @Inject
    public CarDetailsPresenter(DataManager dataManager,
                               SchedulerProvider schedulerProvider,
                               CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }

    @Override
    public void onGetCarDetails(int carId) {

        getMvpView().showLoading();

        getCompositeDisposable().add(getDataManager()
                .getCarDetails(carId)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(new Consumer<CarDetailsResponse>() {
                    @Override
                    public void accept(CarDetailsResponse response) throws Exception {
                        if (!isViewAttached()) {
                            return;
                        }
                        getMvpView().showCarDetails(response);
                        getMvpView().hideLoading();
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {

                        if (!isViewAttached()) {
                            return;
                        }

                        getMvpView().hideLoading();

                        if (throwable instanceof ANError) {
                            ANError anError = (ANError) throwable;
                            handleApiError(anError);
                        }
                    }
                }));
    }

    @Override
    public void onDoQuicRent(int carId) {

        getMvpView().showLoading();

        getCompositeDisposable().add(getDataManager()
                .doQuickRentApiCall(new QuickRentRequest.ServerQuickRentRequest(carId))
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(new Consumer<QuickRentResponse>() {
                    @Override
                    public void accept(QuickRentResponse response) throws Exception {

                        if (!isViewAttached()) {
                            return;
                        }
                        getMvpView().hideLoading();
                        getMvpView().showSuccessQuickRentMessage(response);

                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {

                        if (!isViewAttached()) {
                            return;
                        }

                        getMvpView().hideLoading();

                        if (throwable instanceof ANError) {
                            ANError anError = (ANError) throwable;
                            handleApiError(anError);
                        }
                    }
                }));
    }
}
