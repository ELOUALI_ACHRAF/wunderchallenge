package com.wunder.challenge.mvp.ui.cars;

import android.location.Location;

import com.wunder.challenge.mvp.data.network.model.CarResponse;
import com.wunder.challenge.mvp.ui.base.MvpView;

import java.util.List;

/**
 * Created by ELOUALI ACHRAF on 05/09/2019.
 */

public interface CarsMvpView extends MvpView {

    void addCarsToMap(List<CarResponse> CarMarkers);

    void updateUserLocation(Location userLocation);

}
