package com.wunder.challenge.mvp.utils;

/**
 * Created by ELOUALI on 08/01/17.
 */

public final class AppConstants {

    public static final int API_STATUS_CODE_LOCAL_ERROR = 0;

    public static final String AUTHORIZATION = "Authorization";
    public static final String AUTHORIZATION_TYPE_BEARER = "Bearer";

    public static final String HEADER_ACCEPT = "Accept";
    public static final String HEADER_ACCEPT_FORMAT = "application/json";

    private AppConstants() {
    }
}
