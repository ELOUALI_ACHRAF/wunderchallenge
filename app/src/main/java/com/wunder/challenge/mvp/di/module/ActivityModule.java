package com.wunder.challenge.mvp.di.module;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.LinearLayoutManager;

import com.wunder.challenge.mvp.di.ActivityContext;
import com.wunder.challenge.mvp.di.PerActivity;
import com.wunder.challenge.mvp.ui.cars.CarsMvpPresenter;
import com.wunder.challenge.mvp.ui.cars.CarsMvpView;
import com.wunder.challenge.mvp.ui.cars.CarsPresenter;
import com.wunder.challenge.mvp.ui.details.CarDetailsMvpPresenter;
import com.wunder.challenge.mvp.ui.details.CarDetailsMvpView;
import com.wunder.challenge.mvp.ui.details.CarDetailsPresenter;
import com.wunder.challenge.mvp.ui.splash.SplashMvpPresenter;
import com.wunder.challenge.mvp.ui.splash.SplashMvpView;
import com.wunder.challenge.mvp.ui.splash.SplashPresenter;
import com.wunder.challenge.mvp.utils.rx.AppSchedulerProvider;
import com.wunder.challenge.mvp.utils.rx.SchedulerProvider;

import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;

@Module
public class ActivityModule {

    private FragmentActivity mActivity;

    public ActivityModule(FragmentActivity activity) {
        this.mActivity = activity;
    }

    @Provides
    @ActivityContext
    Context provideContext() {
        return mActivity;
    }

    @Provides
    FragmentActivity provideActivity() {
        return mActivity;
    }

    @Provides
    CompositeDisposable provideCompositeDisposable() {
        return new CompositeDisposable();
    }

    @Provides
    SchedulerProvider provideSchedulerProvider() {
        return new AppSchedulerProvider();
    }

    @Provides
    @PerActivity
    SplashMvpPresenter<SplashMvpView> provideSplashPresenter(
            SplashPresenter<SplashMvpView> presenter) {
        return presenter;
    }


    @Provides
    @PerActivity
    CarDetailsMvpPresenter<CarDetailsMvpView> provideCarDetailsPresenter(
            CarDetailsPresenter<CarDetailsMvpView> presenter) {
        return presenter;
    }


    @Provides
    CarsMvpPresenter<CarsMvpView> provideCarsPresenter(
            CarsPresenter<CarsMvpView> presenter) {
        return presenter;
    }


    @Provides
    LinearLayoutManager provideLinearLayoutManager(FragmentActivity activity) {
        return new LinearLayoutManager(activity);
    }
}
