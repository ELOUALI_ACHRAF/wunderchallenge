package com.wunder.challenge.mvp.ui.splash;


import com.wunder.challenge.mvp.di.PerActivity;
import com.wunder.challenge.mvp.ui.base.MvpPresenter;


@PerActivity
public interface SplashMvpPresenter<V extends SplashMvpView> extends MvpPresenter<V> {

    void onLocationPermissionCheck();

}
