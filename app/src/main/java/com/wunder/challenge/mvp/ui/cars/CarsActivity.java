package com.wunder.challenge.mvp.ui.cars;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.wunder.challenge.mvp.R;
import com.wunder.challenge.mvp.data.network.model.CarResponse;
import com.wunder.challenge.mvp.ui.base.BaseActivity;
import com.wunder.challenge.mvp.ui.details.CarDetailsActivity;
import com.wunder.challenge.mvp.utils.MapUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import butterknife.ButterKnife;

public class CarsActivity extends BaseActivity implements
        CarsMvpView, OnMapReadyCallback, GoogleMap.OnMarkerClickListener, GoogleMap.OnMapClickListener {

    @Inject
    CarsMvpPresenter<CarsMvpView> mPresenter;

    @Inject
    LinearLayoutManager mLayoutManager;


    GoogleMap mGoogleMap;
    boolean mAllMarkersVisible = true;
    Marker userLocationMarker = null;
    boolean userMarkerAdded = false;
    private HashMap<Marker, Integer> mHashMap = new HashMap<>();

    public static Intent getStartIntent(Context context) {
        Intent intent = new Intent(context, CarsActivity.class);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cars);
        getActivityComponent().inject(this);
        setUnBinder(ButterKnife.bind(this));
        mPresenter.onAttach(this);
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

    }

    @Override
    public void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;
        MapUtils.setGoogleMapParams(mGoogleMap, this);

        CarInfoWindowAdapter customInfoWindow = new CarInfoWindowAdapter(this);
        mGoogleMap.setInfoWindowAdapter(customInfoWindow);

        mPresenter.onFetchUserLocation();
        mPresenter.getCarsFromServer();

        mGoogleMap.setOnMarkerClickListener(this);
        mGoogleMap.setOnMapClickListener(this);
    }

    @Override
    public void addCarsToMap(List<CarResponse> carResponseList) {
        mGoogleMap.clear();


        for (int i = 0; i < carResponseList.size(); i++) {
            Marker marker = mGoogleMap.addMarker(MapUtils.carResponse2MarkerOptions(carResponseList.get(i)));
            marker.setTag(carResponseList.get(i));
            marker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.marker_icon));
            mHashMap.put(marker, carResponseList.get(i).getCarId());
        }
        mAllMarkersVisible = true;
    }

    @Override
    public void updateUserLocation(Location userLocation) {
        if (!userMarkerAdded) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    userLocationMarker = mGoogleMap.addMarker(new MarkerOptions()
                            .position(new LatLng(userLocation.getLatitude(), userLocation.getLongitude()))
                            .title(getResources().getString(R.string.your_location_text)));
                    mHashMap.put(userLocationMarker, -1);
                    userMarkerAdded = true;
                    mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(userLocationMarker.getPosition(), 14.0f));
                }
            }, 500);

        } else {
            userLocationMarker.setPosition(new LatLng(userLocation.getLatitude(), userLocation.getLongitude()));
        }
    }

    @Override
    public boolean onMarkerClick(Marker marker) {

        int carId = mHashMap.get(marker);
        if (mAllMarkersVisible) {
            if (carId != -1) {
                for (Map.Entry<Marker, Integer> entry : mHashMap.entrySet()) {
                    if (entry.getValue() != carId && entry.getValue() != -1) {
                        entry.getKey().setVisible(false);
                    }
                }
                zoomOnClickedMarker(marker);
                onMessage(getResources().getString(R.string.show_other_markers_again_indication));
                mAllMarkersVisible = false;
            }
        } else {

            Intent myIntent = new Intent(CarsActivity.this, CarDetailsActivity.class);
            myIntent.putExtra("carId", carId);
            startActivity(myIntent);
            setAllMarkersVisible();
            marker.hideInfoWindow();
        }

        return false;
    }

    @Override
    public void onMapClick(LatLng latLng) {
        setAllMarkersVisible();
    }

    public void setAllMarkersVisible() {
        for (Map.Entry<Marker, Integer> entry : mHashMap.entrySet()) {
            Marker marker = entry.getKey();
            marker.setVisible(true);
        }
        mAllMarkersVisible = true;
    }

    public void zoomOnClickedMarker(Marker marker) {
        new Handler().postDelayed(() -> {
            float zoom = mGoogleMap.getCameraPosition().zoom;
            if (zoom < 14.0f) {
                mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(marker.getPosition(), 14.0f));
            }
        }, 300);
    }

    @Override
    public void onBackPressed() {
        if (mAllMarkersVisible) {
            super.onBackPressed();
        } else {
            setAllMarkersVisible();
        }
    }

}
