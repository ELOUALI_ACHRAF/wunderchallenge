package com.wunder.challenge.mvp.data.network;

import com.rx2androidnetworking.Rx2AndroidNetworking;
import com.wunder.challenge.mvp.BuildConfig;
import com.wunder.challenge.mvp.data.network.model.CarDetailsResponse;
import com.wunder.challenge.mvp.data.network.model.CarResponse;
import com.wunder.challenge.mvp.data.network.model.QuickRentRequest;
import com.wunder.challenge.mvp.data.network.model.QuickRentResponse;
import com.wunder.challenge.mvp.utils.AppConstants;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;
import io.reactivex.Single;


@Singleton
public class AppApiHelper implements ApiHelper {

    @Inject
    public AppApiHelper() {
    }


    @Override
    public Single<QuickRentResponse> doQuickRentApiCall(QuickRentRequest.ServerQuickRentRequest
                                                                request) {

        return Rx2AndroidNetworking.post(BuildConfig.BASE_URL_QUICK_RENT)
                .addHeaders(AppConstants.AUTHORIZATION, AppConstants.AUTHORIZATION_TYPE_BEARER + " " + BuildConfig.SERVER_BEARER_AUTHORIZATION)
                .addHeaders(AppConstants.HEADER_ACCEPT, AppConstants.HEADER_ACCEPT_FORMAT)
                .addJSONObjectBody(request.construtJsonBody())
                .build()
                .getObjectSingle(QuickRentResponse.class);
    }


    @Override
    public Observable<List<CarResponse>> getCarsApiCall() {
        return Rx2AndroidNetworking.get(BuildConfig.BASE_URL_GET_CARS)
                .addHeaders(AppConstants.HEADER_ACCEPT, AppConstants.HEADER_ACCEPT_FORMAT)
                .build()
                .getObjectListObservable(CarResponse.class);
    }

    @Override
    public Single<CarDetailsResponse> getCarDetails(int carId) {
        return Rx2AndroidNetworking.get(BuildConfig.BASE_URL_GET_CARS_DETAILS + carId)
                .addHeaders(AppConstants.HEADER_ACCEPT, AppConstants.HEADER_ACCEPT_FORMAT)
                .build()
                .getObjectSingle(CarDetailsResponse.class);
    }
}

