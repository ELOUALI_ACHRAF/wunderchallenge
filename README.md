



# CONTEXT
-------------------
This repository contains an ANDROID app that is the subject of a Take home test  provided by Wunder Mobility.

# OVERVIEW
-------------------
![Structure](http://achraf.fps-platform.com/wunder/wunder.gif)

# Project Structure
-------------------
![Structure](http://achraf.fps-platform.com/wunder/architecture_wunder.jpg)


# MVP ARCHITECTURE
-------------------
![enter image description here](http://achraf.fps-platform.com/wunder/MVP.jpg)
#### The app has following packages:
1. **data**: It contains all the data accessing and manipulating components.
2. **di**: Dependency providing classes using Dagger2.
3. **ui**: View classes along with their corresponding Presenters.
4. **service**: Services for the application.
5. **utils**: Utility classes.

#### Classes have been designed in such a way that it could be inherited and maximize the code reuse.

# Library reference resources
-------------------
1. Dagger2: [https://github.com/google/dagger](https://github.com/google/dagger)
2. FastAndroidNetworking: [https://github.com/amitshekhariitbhu/Fast-Android-Networking](https://github.com/amitshekhariitbhu/Fast-Android-Networking)
3. Gson : [https://github.com/google/gson](https://github.com/google/gson)
4. Glide : [https://github.com/bumptech/glide](https://github.com/bumptech/glide)
5. PlaceHolderView: https://github.com/janishar/PlaceHolderView
6. ButterKnife: http://jakewharton.github.io/butterknife/
7. RxJava2: [https://github.com/amitshekhariitbhu/RxJava2-Android-Samples](https://github.com/amitshekhariitbhu/RxJava2-Android-Samples)
8.  RxPermission: [https://github.com/vanniktech/RxPermission](https://github.com/vanniktech/RxPermission)
9.  RxLocation:[ https://github.com/abdularis/rxlocation](https://github.com/abdularis/rxlocation)
10. Google Maps : [https://github.com/googlemaps/android-samples](https://github.com/googlemaps/android-samples)

# App features
-------------------
```
1. Launching the App
2. Checking ACCESS_COARSE_LOCATION
3. Fetching User location updates in real time location
4. Getting list of cars from server
5. Rendering cars and user location on top of the map
6. User  click on any car marker
7. Car informations appears
8. The second click redirected to a new activity with more details about the car, information are rendered in a table Layout(Basic informations + Car image + Car location on Map)
9. User do a quick Rent and receives the confirmation message
```

# REMARKS
-------------------
1. Some of cars have a null or empty title, so after the first click the cars ID appears instead of the name.
2. The API responsible of  getting car informations don't work for some IDs and return an Access Denied error ( [https://s3.eu-central-1.amazonaws.com/wunderfleet-recruiting-dev/cars/3](https://s3.eu-central-1.amazonaws.com/wunderfleet-recruiting-dev/cars/3) ).
4. I took the decision to not comment the code for two reasons :
	 - The logic is very easy to understand using the MVP architecture and with resepcting best best practices in methods and variable naming (Feel free to contact me for any details).
	 - Keep the code clean.

# TESTING
-------------------
**Using the MVP architecture makes doing unit test an easy task, i JUnit and MOCKITO to test Model and Presenter Layers

![enter image description here](http://achraf.fps-platform.com/wunder/test_unit.png)

**ESPRESSO was used for UI tests

![enter image description here](http://achraf.fps-platform.com/wunder/test_ui.png)

# TODO
-------------------
1. Handling the config changes using **onSaveInstanceState()** and **ViewModel**.
2. Add some unit tests for the APIs.
3. Optional :  Maybe try the features listed in the **FEATURES** section with another MAP other than GOOGLE MAP : Open Street Map as an example.

# License
-------------------
Licensed under the Apache License, Version 2.0